require 'spec_helper'

describe 'mr-java::default' do
  let(:chef_run) { ChefSpec::Runner.new.converge(described_recipe) }

  it 'should include the oracle java recipe' do
    expect(chef_run).to include_recipe('java::default')
  end
end
