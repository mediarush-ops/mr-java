# A sample Guardfile
# More info at https://github.com/guard/guard#readme

group :development, halt_on_fail: true do
  guard :bundler do
    watch('Gemfile')
    # Uncomment next line if your Gemfile contains the `gemspec' command.
    # watch(/^.+\.gemspec/)
  end

  guard :berkshelf do
    watch('Berksfile')
    watch('metadata.rb')
  end

  guard :rubocop do
    watch(%r{.+\.rb$})
    watch(%r{(?:.+/)?\.rubocop\.yml$}) { |m| File.dirname(m[0]) }
  end

  guard "foodcritic", :cookbook_paths => ".", :all_on_start => false  do
    watch(%r{attributes/.+\.rb$})
    watch(%r{providers/.+\.rb$})
    watch(%r{recipes/.+\.rb$})
    watch(%r{resources/.+\.rb$})
    watch(%r{^templates/(.+)})
    watch('metadata.rb')
  end

  guard :rspec, :cmd => 'bundle exec rspec', :all_on_start => false, notification: false do
    watch(%r{^spec/.+_spec\.rb$})
    watch(%r{^(recipes)/(.+)\.rb$})     { |m| "spec/#{m[1]}_spec.rb" }
    watch('spec/spec_helper.rb')  { "spec" }
  end

  guard 'kitchen' do
    watch(%r{test/.+})
    watch(%r{^recipes/(.+)\.rb$})
    watch(%r{^attributes/(.+)\.rb$})
    watch(%r{^files/(.+)})
    watch(%r{^templates/(.+)})
    watch(%r{^providers/(.+)\.rb})
    watch(%r{^resources/(.+)\.rb})
    watch('metadata.rb')
  end
end
# Add files and commands to this file, like the example:
#   watch(%r{file/path}) { `command(s)` }
#
guard :shell do
  watch(/(.*).txt/) {|m| `tail #{m[0]}` }
end

