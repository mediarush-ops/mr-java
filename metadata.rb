name             'mr-java'
maintainer       'Pritesh Mehta'
maintainer_email 'pritesh@phatforge.com'
license          'All rights reserved'
description      'Installs/Configures mr-java'
long_description 'Installs/Configures mr-java'
version          IO.read(File.join(File.dirname(__FILE__), 'VERSION'))

depends          'java'

recipe           'mr-java::default', 'Installs Oracle 7 Java runtime as Mediarush default'
attribute        'java/jdk_build_version',
  display_name:  'Specific build version for oracle jdk',
  default:       'nil',
  description:   'Build version to use explicitly. Used to construct download url from java cookbook. Ensure you set the checksum'
