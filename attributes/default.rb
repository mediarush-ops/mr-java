
default['java']['jdk_version'] = '7'
default['java']['jdk_build_version'] = nil
default['java']['install_flavor'] = 'oracle'
default['java']['oracle']['accept_oracle_download_terms'] = true

# Allow specifying a specific build
# This doesn't work because of attribute recomputation. (see CHEF-4837)
unless node['java']['jdk_build_version'].nil?
  default['java']['jdk']['7']['x86_64']['url'] = "http://download.oracle.com/otn-pub/java/jdk/#{node['java']['jdk_build_version']}/jdk-#{node['java']['jdk_build_version'].split('-')[0]}-linux-x64.tar.gz"
end
