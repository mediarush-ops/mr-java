require 'spec_helper'

describe 'mr-java' do
  describe file('/usr/lib/jvm/java-7-oracle-amd64/jre/bin/java') do
    it { should be_file }
  end
  describe command('java -version') do
    it { should return_stdout(/1.7.0/) }
  end
end
